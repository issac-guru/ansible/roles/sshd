Ansible Role SSH Daemon Setup for Linux Systems
==============================================

A role for configuring SSH Daemon service on Debian based distributions using [Ansible](http://www.ansible.com)
This Ansible role is part of [Issac Guru Project: Ansible Automation Script for Linux Environments](https://gitlab.com/issac-guru/ansible/roles/).


Features
--------

- TBD


Requirements
------------

- Ansible 2.10.8 or higher
- Debian based distribution (may work with other versions, but has never been tested)


Role Variables
--------------

| VARIABLE                  | DESCRIPTION                                                             |
|---------------------------|-------------------------------------------------------------------------|
|`tbd`                      | To be defined                                                           |

Dependencies
------------

None


Example Playbook
----------------

The following example explains how-to consume ansible role:

```yml
  - name: "TBD"
```


License
-------

The MIT License (MIT)

Copyright (c) 2022 Issac Guru

Premission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnis``hed to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS-IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILIY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


Author Information
------------------

CREATED BY *Issac Nolis Ohasi* \
*LinkedIn Profile:* https://linkedin.com/in/ohasi \
*Gitlab Profile:* https://gitlab.com/ohasi/ \ 
*Website:* https://issac.guru \
*Contact:* me AT issac.guru

